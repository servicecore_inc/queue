<?php

namespace ServiceCore\Queue;

use Laminas\ModuleManager\Feature\ConfigProviderInterface;

class Module implements ConfigProviderInterface
{
    public function getConfig(): array
    {
        $directory = \dirname(__DIR__) . '/config';

        return include $directory . '/module.config.php';
    }
}
