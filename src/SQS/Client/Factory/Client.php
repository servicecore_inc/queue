<?php

namespace ServiceCore\Queue\SQS\Client\Factory;

use Aws\Credentials\Credentials;
use Aws\Sqs\SqsClient;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use ServiceCore\Queue\SQS\Provider\SQS;

class Client implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        ?array $options = null
    ): SqsClient {
        $config     = $container->get('config')['queue']['providers'][SQS::class];
        $connection = $config['connection'];

        if (!\is_array($connection)) {
            throw new \RuntimeException('A connection key is required in the queue config');
        }

        $credentials = null;

        if (\array_key_exists('key', $connection) && \array_key_exists('secret', $connection)) {
            $credentials = new Credentials($connection['key'], $connection['secret']);
        }

        $sqsConfig = [
            'credentials' => $credentials,
            'region'      => $connection['region'],
            'version'     => $connection['version'],
        ];

        return new SqsClient($sqsConfig);
    }
}
