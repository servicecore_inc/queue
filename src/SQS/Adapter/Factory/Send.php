<?php

namespace ServiceCore\Queue\SQS\Adapter\Factory;

use Aws\Sqs\SqsClient;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use RuntimeException;
use ServiceCore\Queue\SQS\Adapter\Send as SendAdapter;
use ServiceCore\Queue\SQS\Provider\SQS;

class Send implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        ?array $options = null
    ): SendAdapter {
        /** @var SqsClient $client */
        $client   = $container->get(SqsClient::class);
        $config   = $container->get('config')['queue']['providers'][SQS::class] ?? [];
        $queue    = $config['queue'] ?? null;
        $endpoint = $config['endpoint'] ?? null;

        if ($queue === null) {
            throw new RuntimeException(\sprintf('Missing `queue` from %s provider', SQS::class));
        }

        if ($endpoint === null) {
            throw new RuntimeException(\sprintf('Missing `endpoint` from %s provider', SQS::class));
        }

        return new SendAdapter($client, $endpoint, $options['connectionParameters'] ?? []);
    }
}
