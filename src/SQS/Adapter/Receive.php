<?php

namespace ServiceCore\Queue\SQS\Adapter;

use Aws\Exception\AwsException;
use Aws\Sqs\SqsClient;
use ServiceCore\Queue\Core\Adapter\AbstractReceive;
use ServiceCore\Queue\Core\Event\ReceivedError;
use Throwable;

class Receive extends AbstractReceive
{
    private SqsClient $sqsClient;

    private string $endpoint;

    public function __construct(SqsClient $sqsClient, string $endpoint, array $connectionParameters = [])
    {
        parent::__construct($connectionParameters);

        $this->sqsClient = $sqsClient;
        $this->endpoint  = $endpoint;
    }

    public function __invoke(string $queueName, bool $forever = true): void
    {
        if ($forever) {
            while ($this->isProcessable()) {
                $this->receive($queueName);
            }
        } else {
            $this->receive($queueName);
        }
    }

    private function receive(string $queueName): void
    {
        $params = \array_merge(
            [
                'AttributeNames'        => ['SentTimestamp'],
                'MaxNumberOfMessages'   => 1,
                'MessageAttributeNames' => ['All'],
                'QueueUrl'              => $this->endpoint . '/' . $queueName,
                'WaitTimeSeconds'       => 10,
            ],
            $this->connectionParameters
        );

        try {
            $result = $this->sqsClient->receiveMessage($params);
        } catch (AwsException $e) {
            $this->events->trigger(ReceivedError::class, $e);

            return;
        }

        if ($result->hasKey('Messages') && \count($result->get('Messages')) > 0) {
            foreach ($result->get('Messages') as $message) {
                if (!\array_key_exists('Body', $message)) {
                    $data = [];
                } else {
                    $data = \json_decode($message['Body'], true);
                }

                $target     = null;
                $properties = [];

                if (
                    \array_key_exists('MessageAttributes', $message)
                    && \is_array($message['MessageAttributes'])
                ) {
                    foreach ($message['MessageAttributes'] as $attributeName => $messageAttribute) {
                        $dataType = $this->parseDataType($messageAttribute['DataType']);

                        if ($attributeName === 'Target') {
                            $target = $messageAttribute[$dataType];

                            unset($message['MessageAttributes'][$attributeName]);

                            continue;
                        }

                        $properties[\lcfirst($attributeName)] = $messageAttribute[$dataType];
                    }
                }

                $receiptHandle = $message['ReceiptHandle'] ?? null;

                if ($target) {
                    $properties['x-message-id'] = $receiptHandle;

                    try {
                        $this->receiveMessage($target, $data, $properties);
                    } catch (Throwable $e) {
                        // delete the message before triggering received error
                        $this->deleteMessage($queueName, $receiptHandle);
                        $this->events->trigger(ReceivedError::class, $e);

                        continue;
                    }
                }

                $this->deleteMessage($queueName, $receiptHandle);
            }
        }
    }

    private function parseDataType(string $dataType): string
    {
        switch ($dataType) {
            case 'String':
                $type = 'StringValue';

                break;
            default:
                $type = 'StringValue';
        }

        return $type;
    }

    private function deleteMessage(string $queueName, ?string $receiptHandle): void
    {
        if ($receiptHandle !== null) {
            $this->sqsClient->deleteMessage(
                [
                    'QueueUrl'      => $this->endpoint . '/' . $queueName,
                    'ReceiptHandle' => $receiptHandle
                ]
            );
        }
    }
}
