<?php

namespace ServiceCore\Queue\SQS\Adapter;

use Aws\Sqs\SqsClient;
use ServiceCore\Queue\Core\Adapter\AbstractSend;
use ServiceCore\Queue\Core\Data\Message;

class Send extends AbstractSend
{
    private SqsClient $sqsClient;

    private string $endpoint;

    public function __construct(SqsClient $sqsClient, string $endpoint, array $connectionParameters = [])
    {
        parent::__construct($connectionParameters);

        $this->sqsClient = $sqsClient;
        $this->endpoint  = $endpoint;
    }

    public function __invoke(string $queueName, string $target, array $data, array $properties = []): void
    {
        $result     = $this->onPreSendMessage();
        $properties = \array_merge($properties, $result['properties']);

        $messageAttributes = [
            'Target' => [
                'DataType'    => 'String',
                'StringValue' => $target,
            ],
        ];

        foreach ($properties as $key => $property) {
            if ($property !== null) {
                $messageAttributes[\ucfirst($key)]['DataType']    = 'String';
                $messageAttributes[\ucfirst($key)]['StringValue'] = (string)$property;
            }
        }

        $params = \array_merge(
            [
                'MessageAttributes' => $messageAttributes,
                'MessageBody'       => \json_encode($data),
                'QueueUrl'          => $this->endpoint . '/' . $queueName
            ],
            $this->connectionParameters
        );

        if (\substr($queueName, -5) === '.fifo') {
            if (\array_key_exists('x-message-group-id', $properties) && $properties['x-message-group-id']) {
                $groupId = $properties['x-message-group-id'];
            } else {
                $groupId = 'default';
            }

            $params['MessageDeduplicationId'] = \uniqid('dedupe', true);
            $params['MessageGroupId']         = $groupId;
        }

        $this->sqsClient->sendMessage($params);

        $message = new Message($target, $data, $properties);

        $this->onPostSendMessage($message);
    }

    public function createQueue(string $queueName): void
    {
        $endpoint = $this->endpoint;

        if (($position = \strrpos($endpoint, '/queue')) !== false) {
            $searchLength = \strlen('/queue');
            $endpoint     = \substr_replace($endpoint, '', $position, $searchLength);
        }

        $this->sqsClient->createQueue(
            [
                'QueueUrl'  => $endpoint,
                'QueueName' => $queueName
            ]
        );
    }
}
