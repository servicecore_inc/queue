<?php

namespace ServiceCore\Queue\SQS\Provider\Factory;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use ServiceCore\Queue\SQS\Adapter\Receive;
use ServiceCore\Queue\SQS\Adapter\Send;
use ServiceCore\Queue\SQS\Provider\SQS as SQSProvider;

class SQS implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        ?array $options = null
    ): SQSProvider {
        $sender   = $container->get(Send::class);
        $receiver = $container->get(Receive::class);

        return new SQSProvider($sender, $receiver);
    }
}
