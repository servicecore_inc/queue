<?php

namespace ServiceCore\Queue\InMemory\Adapter\Factory;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use RuntimeException;
use ServiceCore\Queue\InMemory\Adapter\Send as SendAdapter;
use ServiceCore\Queue\InMemory\Data\MessageQueue;
use ServiceCore\Queue\InMemory\Provider\InMemory;

class Send implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        ?array $options = null
    ): SendAdapter {
        $queueName = $container->get('config')['queue']['providers'][InMemory::class]['queue'] ?? null;

        if ($queueName === null) {
            throw new RuntimeException(\sprintf('Missing `queue` from %s provider', InMemory::class));
        }

        return new SendAdapter(MessageQueue::getInstance(), $queueName);
    }
}
