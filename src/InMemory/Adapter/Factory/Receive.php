<?php

namespace ServiceCore\Queue\InMemory\Adapter\Factory;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use RuntimeException;
use ServiceCore\Queue\InMemory\Adapter\Receive as ReceiveAdapter;
use ServiceCore\Queue\InMemory\Data\MessageQueue;
use ServiceCore\Queue\InMemory\Provider\InMemory;

class Receive implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        ?array $options = null
    ): ReceiveAdapter {
        $queueName = $container->get('config')['queue']['providers'][InMemory::class]['queue'] ?? null;

        if ($queueName === null) {
            throw new RuntimeException(\sprintf('Missing `queue` from %s provider', InMemory::class));
        }

        return new ReceiveAdapter(MessageQueue::getInstance(), $queueName);
    }
}
