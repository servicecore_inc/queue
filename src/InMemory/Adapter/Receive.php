<?php

namespace ServiceCore\Queue\InMemory\Adapter;

use ServiceCore\Queue\Core\Adapter\AbstractReceive;
use ServiceCore\Queue\InMemory\Data\MessageQueue;

class Receive extends AbstractReceive
{
    private MessageQueue $messages;

    public function __construct(MessageQueue $messages)
    {
        parent::__construct();

        $this->messages = $messages;
    }

    public function __invoke(string $queueName, bool $forever): void
    {
        foreach ($this->messages->getAll() as $key => $message) {
            $this->receiveMessage($message['target'], $message['data'], $message['properties']);

            $this->messages->offsetUnset($key);
        }
    }
}
