<?php

namespace ServiceCore\Queue\InMemory\Adapter;

use ServiceCore\Queue\Core\Adapter\AbstractSend;
use ServiceCore\Queue\InMemory\Data\MessageQueue;

class Send extends AbstractSend
{
    private MessageQueue $messages;

    public function __construct(MessageQueue $messages)
    {
        parent::__construct();

        $this->messages = $messages;
    }

    public function __invoke(string $queueName, string $target, array $data, array $properties = []): void
    {
        $this->messages->offsetSet(
            \uniqid('a', false),
            [
                'target'     => $target,
                'data'       => $data,
                'properties' => $properties
            ]
        );
    }
}
