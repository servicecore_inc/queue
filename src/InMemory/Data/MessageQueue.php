<?php

namespace ServiceCore\Queue\InMemory\Data;

use ArrayAccess;

class MessageQueue implements ArrayAccess
{
    /** @var MessageQueue|null */
    private static $instance;

    /** @var array */
    private static $messages = [];

    public static function getInstance(): MessageQueue
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function offsetExists($offset): bool
    {
        return \array_key_exists($offset, self::$messages);
    }

    public function offsetGet($offset)
    {
        return self::$messages[$offset] ?? null;
    }

    public function offsetSet($offset, $value): void
    {
        self::$messages[$offset] = $value;
    }

    public function offsetUnset($offset): void
    {
        unset(self::$messages[$offset]);
    }

    public function getAll(): array
    {
        return self::$messages;
    }

    public function clear(): void
    {
        self::$messages = [];
    }
}
