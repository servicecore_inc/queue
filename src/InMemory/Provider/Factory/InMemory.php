<?php

namespace ServiceCore\Queue\InMemory\Provider\Factory;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use ServiceCore\Queue\InMemory\Adapter\Receive;
use ServiceCore\Queue\InMemory\Adapter\Send;
use ServiceCore\Queue\InMemory\Provider\InMemory as InMemoryProvider;

class InMemory implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        ?array $options = null
    ): InMemoryProvider {
        $sender   = $container->get(Send::class);
        $receiver = $container->get(Receive::class);

        return new InMemoryProvider($sender, $receiver);
    }
}
