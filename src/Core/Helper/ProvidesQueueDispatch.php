<?php

namespace ServiceCore\Queue\Core\Helper;

use RuntimeException;
use ServiceCore\Queue\Core\Provider\Queue as QueueProvider;

trait ProvidesQueueDispatch
{
    public string $queueName;

    private ?QueueProvider $provider = null;

    public function getProvider(): QueueProvider
    {
        if ($this->provider === null) {
            throw new RuntimeException('Provider not set. Hint: use a delegator factory');
        }

        return $this->provider;
    }

    public function setProvider(QueueProvider $provider): self
    {
        $this->provider = $provider;

        return $this;
    }
}
