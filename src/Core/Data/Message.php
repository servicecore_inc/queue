<?php

namespace ServiceCore\Queue\Core\Data;

use JsonSerializable;

class Message implements JsonSerializable
{
    /** @var mixed */
    private $data;

    /** @var string */
    private $target;

    /** @var array */
    private $properties;

    public function __construct(string $target, array $data, array $properties = [])
    {
        $this->target     = $target;
        $this->data       = $data;
        $this->properties = $properties;
    }

    public function getTarget(): string
    {
        return $this->target;
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function getProperties(): array
    {
        return $this->properties;
    }

    public function jsonSerialize(): array
    {
        return [
            'target'     => $this->target,
            'data'       => $this->data,
            'properties' => $this->properties
        ];
    }
}
