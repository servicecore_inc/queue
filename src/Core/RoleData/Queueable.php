<?php

namespace ServiceCore\Queue\Core\RoleData;

use ServiceCore\Queue\Core\Provider\Queue as QueueProvider;

interface Queueable
{
    public function setProvider(QueueProvider $queue);

    public function getProvider(): QueueProvider;
}
