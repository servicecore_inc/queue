<?php

namespace ServiceCore\Queue\Core\Context;

use ServiceCore\Queue\Core\Adapter\AbstractSend;

class Queue
{
    private AbstractSend $send;

    public function __construct(AbstractSend $send)
    {
        $this->send = $send;
    }

    public function queue(
        string $queueName,
        string $target,
        array $data,
        array $properties = []
    ): void {
        $send = $this->send;

        $send($queueName, $target, $data, $properties);
    }
}
