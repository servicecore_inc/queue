<?php

namespace ServiceCore\Queue\Core\Context\Factory;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use ServiceCore\Queue\Core\Context\Dispatch as DispatchContext;
use ServiceCore\Queue\Core\Provider\Queue as QueueProvider;

class Dispatch implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        ?array $options = null
    ): DispatchContext {
        $config = $container->get('config');

        /** @var QueueProvider $queueProvider */
        $queueProvider = $container->get($config['queue']['provider']);

        return new DispatchContext($queueProvider->getReceiver());
    }
}
