<?php

namespace ServiceCore\Queue\Core\Context\Factory;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use ServiceCore\Queue\Core\Context\Queue as QueueContext;
use ServiceCore\Queue\Core\Provider\Queue as QueueProvider;

class Queue implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        ?array $options = null
    ): QueueContext {
        $queueProvider = $container->get('config')['queue']['provider'];

        /** @var QueueProvider $queueProvider */
        $queueProvider = $container->get($queueProvider);

        return new QueueContext($queueProvider->getSender());
    }
}
