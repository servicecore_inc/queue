<?php

namespace ServiceCore\Queue\Core\Context;

use ServiceCore\Queue\Core\Adapter\AbstractReceive;

class Dispatch
{
    /** @var AbstractReceive */
    private $receive;

    public function __construct(AbstractReceive $receive)
    {
        $this->receive = $receive;
    }

    public function dispatch(string $queueName, bool $forever = true): void
    {
        $receive = $this->receive;

        if ($forever) {
            \pcntl_async_signals(true);

            \pcntl_signal(\SIGTERM, [$this, 'gracefulStop']);
            \pcntl_signal(\SIGINT, [$this, 'gracefulStop']);
            \pcntl_signal(\SIGUSR1, [$this, 'gracefulStop']);
        }

        $receive($queueName, $forever);
    }

    public function gracefulStop(): void
    {
        echo "Gracefully stopping worker...\n";

        $this->receive->setIsProcessable(false);
    }
}
