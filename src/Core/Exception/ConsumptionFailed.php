<?php

namespace ServiceCore\Queue\Core\Exception;

use Exception;
use Throwable;

class ConsumptionFailed extends Exception
{
    public function __construct(string $message, ?Throwable $previous = null)
    {
        parent::__construct($message, 422, $previous);
    }
}
