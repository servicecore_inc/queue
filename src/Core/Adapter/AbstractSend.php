<?php

namespace ServiceCore\Queue\Core\Adapter;

use Laminas\EventManager\EventManagerAwareInterface;
use Laminas\EventManager\EventManagerAwareTrait;
use ServiceCore\Queue\Core\Data\Message;
use ServiceCore\Queue\Core\Event\PreQueued;
use ServiceCore\Queue\Core\Event\Queued;

abstract class AbstractSend implements EventManagerAwareInterface
{
    use EventManagerAwareTrait;

    protected array $connectionParameters;

    public function __construct(array $connectionParameters = [])
    {
        $this->connectionParameters = $connectionParameters;
    }

    abstract public function __invoke(
        string $queueName,
        string $target,
        array $data,
        array $properties = []
    ): void;

    protected function onPreSendMessage(): array
    {
        $preQueued = new PreQueued();

        $this->events->trigger(PreQueued::class, $preQueued);

        return [
            'properties' => $preQueued->getParams()
        ];
    }

    /** @deprecated in favor of onPostSendMessage() */
    protected function sendMessage(Message $message): void
    {
        $this->events->trigger(Queued::class, new Queued($message));
    }

    protected function onPostSendMessage(Message $message): void
    {
        $this->events->trigger(Queued::class, new Queued($message));
    }
}
