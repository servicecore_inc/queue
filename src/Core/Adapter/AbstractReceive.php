<?php

namespace ServiceCore\Queue\Core\Adapter;

use Laminas\EventManager\EventManagerAwareInterface;
use Laminas\EventManager\EventManagerAwareTrait;
use ServiceCore\Queue\Core\Data\Message;
use ServiceCore\Queue\Core\Event\PostReceived;
use ServiceCore\Queue\Core\Event\Received;
use ServiceCore\Queue\Core\Event\ReceivedError;
use ServiceCore\Queue\Core\Exception\ConsumptionFailed;

abstract class AbstractReceive implements EventManagerAwareInterface
{
    use EventManagerAwareTrait;

    protected array $connectionParameters;

    private bool $isProcessable = true;

    public function __construct(array $connectionParameters = [])
    {
        $this->connectionParameters = $connectionParameters;
    }

    abstract public function __invoke(string $queueName, bool $forever): void;

    public function isProcessable(): bool
    {
        return $this->isProcessable;
    }

    public function setIsProcessable(bool $isProcessable): self
    {
        $this->isProcessable = $isProcessable;

        return $this;
    }

    protected function receiveMessage(string $target, array $data = [], array $properties = []): void
    {
        $queueMessage = new Message($target, $data, $properties);
        $received     = new Received($queueMessage);

        // @TODO Consider changing this to be a triggerUntil() ConsumptionFailed
        $responses = $this->events->trigger(Received::class, $received);

        foreach ($responses as $response) {
            if ($response instanceof ConsumptionFailed) {
                $this->events->trigger(ReceivedError::class, $response);

                break;
            }
        }

        $this->events->trigger(PostReceived::class, $received);
    }
}
