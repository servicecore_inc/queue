<?php

namespace ServiceCore\Queue\Core\Event;

use Laminas\EventManager\Event;
use ServiceCore\Queue\Core\Data\Message;

class PostReceived extends Event
{
    public function __construct(Message $message)
    {
        parent::__construct(
            self::class,
            $message->getTarget(),
            $message->getData()
        );
    }
}
