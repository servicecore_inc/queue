<?php

namespace ServiceCore\Queue\Core\Event;

use Laminas\EventManager\Event;

class PreQueued extends Event
{
    public function __construct()
    {
        parent::__construct(self::class, self::class);
    }
}
