<?php

namespace ServiceCore\Queue\Core\Event;

use Laminas\EventManager\Event;
use ServiceCore\Queue\Core\Data\Message;

class Received extends Event
{
    /** @var array */
    private $properties;

    public function __construct(Message $message)
    {
        $this->properties = $message->getProperties();

        parent::__construct(
            self::class,
            $message->getTarget(),
            $message->getData()
        );
    }

    public function getProperties(): array
    {
        return $this->properties;
    }

    /**
     * @param string     $name
     * @param mixed|null $default
     *
     * @return mixed
     */
    public function getProperty(string $name, $default = null)
    {
        return $this->properties[$name] ?? $default;
    }
}
