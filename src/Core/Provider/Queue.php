<?php

namespace ServiceCore\Queue\Core\Provider;

use ServiceCore\Queue\Core\Adapter\AbstractReceive;
use ServiceCore\Queue\Core\Adapter\AbstractSend;

abstract class Queue
{
    /** @var AbstractSend */
    protected $send;

    /** @var AbstractReceive */
    protected $receive;

    public function __construct(AbstractSend $send, AbstractReceive $receive)
    {
        $this->send    = $send;
        $this->receive = $receive;
    }

    public function getSender(): AbstractSend
    {
        return $this->send;
    }

    public function getReceiver(): AbstractReceive
    {
        return $this->receive;
    }
}
