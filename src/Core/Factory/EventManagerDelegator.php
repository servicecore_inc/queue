<?php

namespace ServiceCore\Queue\Core\Factory;

use Interop\Container\ContainerInterface;
use Laminas\EventManager\EventManagerAwareInterface;
use Laminas\EventManager\EventManagerInterface;
use Laminas\ServiceManager\Factory\DelegatorFactoryInterface;
use RuntimeException;
use ServiceCore\Queue\Core\EventManager;

class EventManagerDelegator implements DelegatorFactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $name,
        callable $callback,
        ?array $options = null
    ): object {
        $class = $callback();

        if (!$class instanceof EventManagerAwareInterface) {
            throw new RuntimeException(
                \sprintf(
                    'Class %s does not implement %s',
                    \get_class($class),
                    EventManagerAwareInterface::class
                )
            );
        }

        /** @var EventManagerInterface $eventManager */
        $eventManager = $container->get(EventManager::class);

        $class->setEventManager($eventManager);

        return $class;
    }
}
