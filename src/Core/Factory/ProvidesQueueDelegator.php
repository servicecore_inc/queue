<?php

namespace ServiceCore\Queue\Core\Factory;

use Interop\Container\ContainerInterface;
use InvalidArgumentException;
use Laminas\ServiceManager\Factory\DelegatorFactoryInterface;
use ServiceCore\Queue\Core\RoleData\Queueable;

class ProvidesQueueDelegator implements DelegatorFactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $name,
        callable $callback,
        ?array $options = null
    ): Queueable {
        $class = $callback();

        if (!$class instanceof Queueable) {
            throw new InvalidArgumentException(
                \sprintf(
                    'Class %s does not implement interface %s',
                    \get_class($class),
                    Queueable::class
                )
            );
        }

        $config = $container->get('config');

        $class->setProvider($container->get($config['queue']['provider']));

        return $class;
    }
}
