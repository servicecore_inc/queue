<?php

namespace ServiceCore\Queue\RabbitMQ\Adapter;

use PhpAmqpLib\Connection\AbstractConnection;
use PhpAmqpLib\Message\AMQPMessage;
use RuntimeException;
use ServiceCore\Queue\Core\Adapter\AbstractSend;
use ServiceCore\Queue\Core\Data\Message;

class Send extends AbstractSend
{
    private ?AbstractConnection $connection;

    public function __construct(?AbstractConnection $connection = null)
    {
        parent::__construct();

        $this->connection = $connection;
    }

    public function __invoke(string $queueName, string $target, array $data, array $properties = []): void
    {
        if (!$this->connection instanceof AbstractConnection) {
            throw new RuntimeException('Failed establishing connection to Rabbit.');
        }

        $result     = $this->onPreSendMessage();
        $properties = \array_merge($properties, $result['properties']);

        $queueMessage = new Message($target, $data, $properties);
        $message      = new AMQPMessage(\json_encode($queueMessage));
        $channel      = $this->connection->channel();

        $channel->queue_declare($queueName, false, true, false, false);
        $channel->basic_publish($message, '', $queueName);

        $this->onPostSendMessage($queueMessage);
    }

    public function purgeQueue(string $queueName): void
    {
        if (!$this->connection instanceof AbstractConnection) {
            throw new RuntimeException('Failed establishing connection to Rabbit.');
        }

        $channel = $this->connection->channel();

        $channel->queue_purge($queueName, true);
    }
}
