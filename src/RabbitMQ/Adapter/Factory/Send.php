<?php

namespace ServiceCore\Queue\RabbitMQ\Adapter\Factory;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use PhpAmqpLib\Connection\AMQPLazyConnection;
use RuntimeException;
use ServiceCore\Queue\RabbitMQ\Adapter\Send as SendAdapter;
use ServiceCore\Queue\RabbitMQ\Provider\RabbitMQ;

class Send implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        ?array $options = null
    ): SendAdapter {
        try {
            /** @var AMQPLazyConnection $connection */
            $connection = $container->get(AMQPLazyConnection::class);
        } catch (\Throwable $e) {
            $connection = null;
        }

        $queueName = $container->get('config')['queue']['providers'][RabbitMQ::class]['queue'] ?? null;

        if ($queueName === null) {
            throw new RuntimeException(\sprintf('Missing `queue` from %s provider', RabbitMQ::class));
        }

        return new SendAdapter($queueName, $connection);
    }
}
