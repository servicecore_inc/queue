<?php

namespace ServiceCore\Queue\RabbitMQ\Adapter;

use Closure;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AbstractConnection;
use PhpAmqpLib\Message\AMQPMessage;
use RuntimeException;
use ServiceCore\Queue\Core\Adapter\AbstractReceive;
use ServiceCore\Queue\Core\Event\ReceivedError;
use Throwable;

class Receive extends AbstractReceive
{
    private AbstractConnection $connection;

    public function __construct(AbstractConnection $connection)
    {
        parent::__construct();

        $this->connection = $connection;
    }

    public function __invoke(string $queueName, bool $forever = true): void
    {
        $channel = $this->connection->channel();

        if (!$channel instanceof AMQPChannel) {
            throw new RuntimeException('Failed establishing Rabbit channel.');
        }

        $this->setupQueue($queueName, $channel);

        if ($forever) {
            while (\count($channel->callbacks) && $this->isProcessable()) {
                $channel->wait();
            }
        } else {
            $channel->wait(null, true);
        }

        $channel->close();
    }

    private function setupQueue(string $queueName, AMQPChannel $channel): void
    {
        $callback = $this->getCallback($channel);

        $channel->queue_declare($queueName, false, true, false, false);
        $channel->basic_consume($queueName, '', false, false, false, false, $callback);
    }

    private function getCallback(AMQPChannel $channel): Closure
    {
        return function (AMQPMessage $message) use ($channel) {
            try {
                \ini_set('memory_limit', '2G');

                $decodedBody = \json_decode($message->body, true);
                $properties  = \array_merge($decodedBody['properties'] ?? [], $message->delivery_info);

                $this->receiveMessage($decodedBody['target'], $decodedBody['data'] ?? [], $properties);

                $channel->basic_ack($message->delivery_info['delivery_tag']);
            } catch (Throwable $e) {
                $channel->basic_ack($message->delivery_info['delivery_tag']);

                $this->events->trigger(ReceivedError::class, $e);
            }
        };
    }
}
