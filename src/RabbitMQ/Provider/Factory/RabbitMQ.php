<?php

namespace ServiceCore\Queue\RabbitMQ\Provider\Factory;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use ServiceCore\Queue\RabbitMQ\Adapter\Receive;
use ServiceCore\Queue\RabbitMQ\Adapter\Send;
use ServiceCore\Queue\RabbitMQ\Provider\RabbitMQ as RabbitMQProvider;

class RabbitMQ implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        ?array $options = null
    ): RabbitMQProvider {
        $sender   = $container->get(Send::class);
        $receiver = $container->get(Receive::class);

        return new RabbitMQProvider($sender, $receiver);
    }
}
