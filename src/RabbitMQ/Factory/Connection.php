<?php

namespace ServiceCore\Queue\RabbitMQ\Factory;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use PhpAmqpLib\Connection\AMQPLazyConnection;
use RuntimeException;
use ServiceCore\Queue\RabbitMQ\Provider\RabbitMQ;

class Connection implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        ?array $options = null
    ): AMQPLazyConnection {
        $config   = $container->get('config');
        $host     = $config['queue']['providers'][RabbitMQ::class]['connection']['host'] ?? null;
        $port     = $config['queue']['providers'][RabbitMQ::class]['connection']['port'] ?? null;
        $user     = $config['queue']['providers'][RabbitMQ::class]['connection']['user'] ?? null;
        $password = $config['queue']['providers'][RabbitMQ::class]['connection']['password'] ?? null;

        if ($host === null || $port === null || $user === null || $password === null) {
            throw new RuntimeException('Proper RabbitMQ connection keys are not present');
        }

        return new AMQPLazyConnection($host, $port, $user, $password);
    }
}
