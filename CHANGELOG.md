# Changelog

All notable changes to this project will be documented in this file, in reverse chronological order by release.

## 4.2.0 - 2024-03-07

### Added

- onPreSendMessage event to senders

### Deprecated

- sendMessage on senders

## 4.1.0 - 2023-09-07

### Changed

- Updated message group property name from x-company-id to x-message-group-id

## 4.0.8 - 2023-09-06

### Fixed

- ReceivedError event is triggered instead of re-throwing exception

## 4.0.7 - 2023-09-06

### Added

- x-message-id to SQS parameters
- Support to delete messages if exceptions are thrown in receiveMessage
- Lintint!

## 4.0.6 - 2023-08-29

### Added

- Support FIFO queues by conditionally adding required message parameters

## 4.0.5 - 2023-05-12

### Added

- Support passing previous Throwable in ConsumptionFailed

## 4.0.4 - 2022-10-18

### Added

- Default queue name property to ProvidesQueueDispatch

## 4.0.3 - 2022-10-18

### Added

- Connection parameters to SQS Receive and Send context factory options

## 4.0.2 - 2022-10-18

### Added

- Connection parameters to all Receive and Send contexts to customize connection options

## 4.0.1 - 2022-10-18

### Fixed

- Dispatch and send contexts missing queueName argument

## 4.0.0 - 2022-08-01

### Added

- queueName argument to AbstractSend __invoke signature
- queueName argument to AbstractReceive __invoke signature

### Changed

- Minimum PHP version to 7.4

### Removed

- Getter and setter for queueName in AbstractSend and AbstractReceive
- queueName from AbstractSend constructor
- queueName from AbstractReceive constructor

## 3.1.1 - 2022-07-29

### Added

- Support for instance profiles in SQS connections

## 3.x.x - 2020-xx-xx

### Added

- InMemory adapter

### Changed

- Update library to support zend -> laminas rename

## 1.0.1 - 2019-11-15

### Added

- Signal handling for SIGINT, SIGTERM, and SIGUSR1

## 1.0.0 - 2018-10-23

### Added

- Adapters, factories, listeners, exceptions, interfaces, tests
