<?php

use Aws\Sqs\SqsClient as SQSClient;
use Laminas\ServiceManager\Factory\InvokableFactory;
use PhpAmqpLib\Connection\AMQPLazyConnection;
use ServiceCore\Queue\Core\Context\Dispatch as DispatchContext;
use ServiceCore\Queue\Core\Context\Factory\Dispatch as DispatchContextFactory;
use ServiceCore\Queue\Core\Context\Factory\Queue as QueueContextFactory;
use ServiceCore\Queue\Core\Context\Queue as QueueContext;
use ServiceCore\Queue\Core\EventManager as QueueEventManager;
use ServiceCore\Queue\Core\Factory\EventManagerDelegator;
use ServiceCore\Queue\InMemory\Adapter\Factory\Receive as InMemoryReceiveAdapterFactory;
use ServiceCore\Queue\InMemory\Adapter\Factory\Send as InMemorySendAdapterFactory;
use ServiceCore\Queue\InMemory\Adapter\Receive as InMemoryReceiveAdapter;
use ServiceCore\Queue\InMemory\Adapter\Send as InMemorySendAdapter;
use ServiceCore\Queue\InMemory\Provider\Factory\InMemory as InMemoryProviderFactory;
use ServiceCore\Queue\InMemory\Provider\InMemory as InMemoryProvider;
use ServiceCore\Queue\RabbitMQ\Adapter\Factory\Receive as RabbitMQReceiveAdapterFactory;
use ServiceCore\Queue\RabbitMQ\Adapter\Factory\Send as RabbitMQSendAdapterFactory;
use ServiceCore\Queue\RabbitMQ\Adapter\Receive as RabbitMQReceiveAdapter;
use ServiceCore\Queue\RabbitMQ\Adapter\Send as RabbitMQSendAdapter;
use ServiceCore\Queue\RabbitMQ\Factory\Connection as RabbitMQConnectionFactory;
use ServiceCore\Queue\RabbitMQ\Provider\Factory\RabbitMQ as RabbitMQProviderFactory;
use ServiceCore\Queue\RabbitMQ\Provider\RabbitMQ as RabbitMQProvider;
use ServiceCore\Queue\SQS\Adapter\Factory\Receive as SQSReceiveAdapterFactory;
use ServiceCore\Queue\SQS\Adapter\Factory\Send as SQSSendAdapterFactory;
use ServiceCore\Queue\SQS\Adapter\Receive as SQSReceiveAdapter;
use ServiceCore\Queue\SQS\Adapter\Send as SQSSendAdapter;
use ServiceCore\Queue\SQS\Client\Factory\Client as SQSClientFactory;
use ServiceCore\Queue\SQS\Provider\Factory\SQS as SQSProviderFactory;
use ServiceCore\Queue\SQS\Provider\SQS as SQSProvider;

return [
    'service_manager' => [
        'factories'  => [
            DispatchContext::class        => DispatchContextFactory::class,
            AMQPLazyConnection::class     => RabbitMQConnectionFactory::class,
            SQSReceiveAdapter::class      => SQSReceiveAdapterFactory::class,
            SQSSendAdapter::class         => SQSSendAdapterFactory::class,
            RabbitMQReceiveAdapter::class => RabbitMQReceiveAdapterFactory::class,
            RabbitMQSendAdapter::class    => RabbitMQSendAdapterFactory::class,
            SQSClient::class              => SQSClientFactory::class,
            QueueContext::class           => QueueContextFactory::class,
            SQSProvider::class            => SQSProviderFactory::class,
            RabbitMQProvider::class       => RabbitMQProviderFactory::class,
            InMemoryProvider::class       => InMemoryProviderFactory::class,
            InMemorySendAdapter::class    => InMemorySendAdapterFactory::class,
            InMemoryReceiveAdapter::class => InMemoryReceiveAdapterFactory::class,
            QueueEventManager::class      => InvokableFactory::class
        ],
        'delegators' => [
            SQSReceiveAdapter::class      => [
                EventManagerDelegator::class
            ],
            RabbitMQReceiveAdapter::class => [
                EventManagerDelegator::class
            ],
            SQSSendAdapter::class         => [
                EventManagerDelegator::class
            ],
            RabbitMQSendAdapter::class    => [
                EventManagerDelegator::class
            ],
            InMemorySendAdapter::class    => [
                EventManagerDelegator::class
            ],
            InMemoryReceiveAdapter::class => [
                EventManagerDelegator::class
            ]
        ],
    ],
];
