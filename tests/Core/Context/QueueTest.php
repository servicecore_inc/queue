<?php

namespace ServiceCore\Queue\Test\Core\Context;

use PHPUnit\Framework\TestCase;
use ServiceCore\Queue\Core\Adapter\AbstractSend;
use ServiceCore\Queue\Core\Context\Queue;

class QueueTest extends TestCase
{
    public function testQueue(): void
    {
        $send     = $this->getMockSend();
        $dispatch = new Queue($send);
        $target   = 'test.queue';
        $data     = ['data' => 1];

        $dispatch->queue('testQueue', $target, $data, []);
    }

    private function getMockSend(): AbstractSend
    {
        $builder = $this->getMockBuilder(AbstractSend::class);

        $builder->setMethods(
            [
                '__invoke',
            ]
        );

        $builder->disableOriginalConstructor();

        $mock = $builder->getMockForAbstractClass();

        $mock->expects($this->once())
             ->method('__invoke');

        return $mock;
    }
}
