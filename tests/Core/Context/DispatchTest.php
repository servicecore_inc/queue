<?php

namespace ServiceCore\Queue\Test\Core\Context;

use PHPUnit\Framework\TestCase;
use ServiceCore\Queue\Core\Adapter\AbstractReceive;
use ServiceCore\Queue\Core\Context\Dispatch;

class DispatchTest extends TestCase
{
    public function testDispatch(): void
    {
        $receive = $this->getMockForAbstractClass(
            AbstractReceive::class,
            [],
            '',
            false,
            true,
            true,
            [
                '__invoke',
            ]
        );

        $receive->expects($this->once())
            ->method('__invoke');

        $dispatch = new Dispatch($receive);

        $dispatch->dispatch('test.queue');
    }

    public function testGracefulStop(): void
    {
        $receive = $this->getMockForAbstractClass(
            AbstractReceive::class,
            [],
            '',
            false,
            true,
            true,
            [
                'setIsProcessable',
            ]
        );

        $receive->expects($this->once())
            ->method('setIsProcessable')
            ->willReturnSelf();

        $dispatch = new Dispatch($receive);

        $dispatch->gracefulStop();
    }
}
