<?php

namespace ServiceCore\Queue\Test\Core\Event;

use PHPUnit\Framework\TestCase;
use ServiceCore\Queue\Core\Data\Message;
use ServiceCore\Queue\Core\Event\ReceivedError;

class ReceivedErrorTest extends TestCase
{
    public function testGetName(): void
    {
        $target  = 'test.queue';
        $data    = ['test' => true];
        $message = new Message($target, $data);
        $event   = new ReceivedError($message);

        $this->assertEquals(ReceivedError::class, $event->getName());
    }

    public function testGetTarget(): void
    {
        $target  = 'test.queue';
        $data    = ['test' => true];
        $message = new Message($target, $data);
        $event   = new ReceivedError($message);

        $this->assertEquals($message, $event->getTarget());
    }

    public function testGetParams(): void
    {
        $target  = 'test.queue';
        $data    = ['test' => true];
        $message = new Message($target, $data);
        $event   = new ReceivedError($message);

        $this->assertEmpty($event->getParams());
    }
}
