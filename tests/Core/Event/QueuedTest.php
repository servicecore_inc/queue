<?php

namespace ServiceCore\Queue\Test\Core\Event;

use PHPUnit\Framework\TestCase;
use ServiceCore\Queue\Core\Data\Message;
use ServiceCore\Queue\Core\Event\Queued;

class QueuedTest extends TestCase
{
    public function testGetName(): void
    {
        $target  = 'test.queue';
        $data    = ['test' => true];
        $message = new Message($target, $data);
        $event   = new Queued($message);

        $this->assertEquals(Queued::class, $event->getName());
    }

    public function testGetTarget(): void
    {
        $target  = 'test.queue';
        $data    = ['test' => true];
        $message = new Message($target, $data);
        $event   = new Queued($message);

        $this->assertEquals($target, $event->getTarget());
    }

    public function testGetParams(): void
    {
        $target  = 'test.queue';
        $data    = ['test' => true];
        $message = new Message($target, $data);
        $event   = new Queued($message);

        $this->assertEquals($data, $event->getParams());
    }
}
