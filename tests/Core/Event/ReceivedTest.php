<?php

namespace ServiceCore\Queue\Test\Core\Event;

use PHPUnit\Framework\TestCase;
use ServiceCore\Queue\Core\Data\Message;
use ServiceCore\Queue\Core\Event\Received;

class ReceivedTest extends TestCase
{
    public function testGetName(): void
    {
        $target  = 'test.queue';
        $data    = ['test' => true];
        $message = new Message($target, $data);
        $event   = new Received($message);

        $this->assertEquals(Received::class, $event->getName());
    }

    public function testGetTarget(): void
    {
        $target  = 'test.queue';
        $data    = ['test' => true];
        $message = new Message($target, $data);
        $event   = new Received($message);

        $this->assertEquals($target, $event->getTarget());
    }

    public function testGetParams(): void
    {
        $target  = 'test.queue';
        $data    = ['test' => true];
        $message = new Message($target, $data);
        $event   = new Received($message);

        $this->assertEquals($data, $event->getParams());
    }

    public function testGetProperties(): void
    {
        $target     = 'test.queue';
        $data       = ['test' => true];
        $properties = ['test' => true];
        $message    = new Message($target, $data, $properties);
        $event      = new Received($message);

        $this->assertEquals($properties, $event->getProperties());
    }

    public function testGetPropertiesEmpty(): void
    {
        $target  = 'test.queue';
        $data    = ['test' => true];
        $message = new Message($target, $data);
        $event   = new Received($message);

        $this->assertEmpty($event->getProperties());
    }
}
