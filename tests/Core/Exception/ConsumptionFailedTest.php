<?php

namespace ServiceCore\Queue\Test\Core\Exception;

use PHPUnit\Framework\TestCase;
use ServiceCore\Queue\Core\Exception\ConsumptionFailed;

class ConsumptionFailedTest extends TestCase
{
    public function testGetMessage(): void
    {
        $message   = 'failed yo!';
        $exception = new ConsumptionFailed($message);

        $this->assertEquals($message, $exception->getMessage());
    }

    public function testGetCode(): void
    {
        $exception = new ConsumptionFailed('test');

        $this->assertEquals(422, $exception->getCode());
    }

    public function testGetPrevious(): void
    {
        $previous  = new \Exception('previous yo!');
        $exception = new ConsumptionFailed('test', $previous);

        $this->assertEquals($previous, $exception->getPrevious());
    }
}
