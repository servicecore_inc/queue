<?php

namespace ServiceCore\Queue\Test\Core\Data;

use PHPUnit\Framework\TestCase;
use ServiceCore\Queue\Core\Data\Message;

class MessageTest extends TestCase
{
    public function testGetTarget(): void
    {
        $target  = 'test.queue';
        $message = new Message($target, [], []);

        $this->assertEquals($target, $message->getTarget());
    }

    public function testGetData(): void
    {
        $data    = ['test' => true];
        $message = new Message('target', $data, []);

        $this->assertEquals($data, $message->getData());
    }

    public function testGetProperties(): void
    {
        $properties = ['test' => true];
        $message    = new Message('target', [], $properties);

        $this->assertEquals($properties, $message->getProperties());
    }

    public function testGetPropertiesEmpty(): void
    {
        $message = new Message('target', []);

        $this->assertEmpty($message->getProperties());
    }

    public function testJsonSerialize(): void
    {
        $target     = 'test.queue';
        $data       = ['test' => true];
        $properties = ['test' => true];
        $message    = new Message($target, $data, $properties);
        $serialized = $message->jsonSerialize();

        $this->assertNotEmpty($serialized);

        $this->assertArrayHasKey('target', $serialized);
        $this->assertArrayHasKey('data', $serialized);
        $this->assertArrayHasKey('properties', $serialized);

        $this->assertEquals($target, $serialized['target']);
        $this->assertEquals($data, $serialized['data']);
        $this->assertEquals($properties, $serialized['properties']);
    }
}
