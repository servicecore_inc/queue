<?php

namespace ServiceCore\Queue\Test\RabbitMQ\Adapter;

use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AbstractConnection;
use PHPUnit\Framework\TestCase;
use RuntimeException;
use ServiceCore\Queue\Core\EventManager;
use ServiceCore\Queue\RabbitMQ\Adapter\Send;

class SendTest extends TestCase
{
    public function testInvoke(): void
    {
        $channelBuilder = $this->getMockBuilder(AMQPChannel::class);

        $channelBuilder->disableOriginalConstructor();

        $channelBuilder->setMethods(
            [
                'queue_declare',
                'basic_publish'
            ]
        );

        $channel = $channelBuilder->getMock();

        $channel->expects($this->once())
                ->method('queue_declare')
                ->willReturn(true);

        $channel->expects($this->once())
                ->method('basic_publish')
                ->willReturn(true);

        $connection = $this->getMockConnection($channel);
        $send       = new Send($connection);
        $evmBuilder = $this->getMockBuilder(EventManager::class);

        $evmBuilder->disableOriginalConstructor();

        $evmBuilder->setMethods(
            [
                'trigger'
            ]
        );

        $events = $evmBuilder->getMock();

        $events->expects($this->exactly(2))
               ->method('trigger')
               ->willReturn(true);

        $send->setEventManager($events);

        $data = ['test' => true];

        $send('queue.test', 'target', $data);
    }

    public function testInvokeThrowsException(): void
    {
        $send = new Send(null);

        $this->expectException(RuntimeException::class);

        $send('queue.test', 'target', []);
    }

    public function testPurgeQueue(): void
    {
        $channelBuilder = $this->getMockBuilder(AMQPChannel::class);

        $channelBuilder->disableOriginalConstructor();

        $channelBuilder->setMethods(
            [
                'queue_purge',
            ]
        );

        $channel = $channelBuilder->getMock();

        $channel->expects($this->once())
                ->method('queue_purge')
                ->willReturn(true);

        $connection = $this->getMockConnection($channel);
        $send       = new Send($connection);

        $send->purgeQueue('queue.test');
    }

    public function testPurgeQueueThrowsException(): void
    {
        $send = new Send(null);

        $this->expectException(RuntimeException::class);

        $send->purgeQueue('queue.test');
    }

    private function getMockConnection(AMQPChannel $channel): AbstractConnection
    {
        $connectionBuilder = $this->getMockBuilder(AbstractConnection::class);

        $connectionBuilder->disableOriginalConstructor();

        $connectionBuilder->setMethods(
            [
                'channel'
            ]
        );

        $connection = $connectionBuilder->getMockForAbstractClass();

        $connection->expects($this->once())
                   ->method('channel')
                   ->willReturn($channel);

        return $connection;
    }
}
