<?php

namespace ServiceCore\Queue\Test\RabbitMQ\Adapter;

use Laminas\EventManager\ResponseCollection;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AbstractConnection;
use PhpAmqpLib\Message\AMQPMessage;
use PHPUnit\Framework\TestCase;
use ServiceCore\Queue\Core\Event\PostReceived;
use ServiceCore\Queue\Core\Event\Received;
use ServiceCore\Queue\Core\EventManager;
use ServiceCore\Queue\RabbitMQ\Adapter\Receive;

class ReceiveTest extends TestCase
{
    public function testInvoke(): void
    {
        $forever    = false;
        $target     = 'queue.test';
        $connection = $this->getMockConnection($forever);
        $receive    = new Receive($connection);
        $evmBuilder = $this->getMockBuilder(EventManager::class);

        $evmBuilder->disableOriginalConstructor();
        $evmBuilder->setMethods(
            [
                'trigger'
            ]
        );

        $eventManager = $evmBuilder->getMock();

        $eventManager->expects($this->exactly(2))
                     ->method('trigger')
                     ->withConsecutive(
                         [
                             Received::class,
                         ],
                         [
                             PostReceived::class,
                         ]
                     )
                     ->willReturn(new ResponseCollection());

        $receive->setEventManager($eventManager);

        $receive($target, $forever);
    }

    public function testInvokeForever(): void
    {
        $forever    = true;
        $connection = $this->getMockConnection($forever);
        $evmBuilder = $this->getMockBuilder(EventManager::class);

        $evmBuilder->disableOriginalConstructor();
        $evmBuilder->setMethods(
            [
                'trigger'
            ]
        );

        $eventManager = $evmBuilder->getMock();

        $eventManager->expects($this->exactly(2))
                     ->method('trigger')
                     ->willReturn(new ResponseCollection());


        $receive = new Receive($connection);

        $receive->setEventManager($eventManager);

        $receive('queue.test', $forever);
    }

    private function getMockConnection(bool $forever): AbstractConnection
    {
        $connectionBuilder = $this->getMockBuilder(AbstractConnection::class);

        $connectionBuilder->disableOriginalConstructor();

        $connectionBuilder->setMethods(
            [
                'channel'
            ]
        );

        $connection = $connectionBuilder->getMockForAbstractClass();

        $channelBuilder = $this->getMockBuilder(AMQPChannel::class);

        $channelBuilder->disableOriginalConstructor();

        $channelBuilder->setMethods(
            [
                'wait',
                'queue_declare',
                'basic_consume',
                'basic_ack'
            ]
        );

        $channel = $channelBuilder->getMock();

        $channel->expects($this->once())
                ->method('queue_declare')
                ->willReturn(true);

        $channel->expects($this->once())
                ->method('basic_ack')
                ->willReturn(true);

        $channel->expects($this->once())
                ->method('basic_consume')
                 ->willReturnCallback(static function (
                     string $queueName,
                     string $consumerTag,
                     bool $noLocal,
                     bool $noAck,
                     bool $exclusive,
                     bool $noWait,
                     callable $callback
                 ) {
                     $body                   = ['properties' => [], 'target' => 'abc', 'data' => []];
                     $message                = new AMQPMessage(\json_encode($body));
                     $message->delivery_info = [
                         'delivery_tag' => 'abc123'
                     ];

                     $callback($message);
                 });

        if ($forever) {
            $channel->callbacks = [1];

            $channel->expects($this->once())
                    ->method('wait')
                    ->willReturnCallback(static function () use (&$channel) {
                        $channel->callbacks = [];
                    });
        } else {
            $channel->expects($this->once())
                    ->method('wait')
                    ->willReturn(true);
        }

        $connection->expects($this->once())
                   ->method('channel')
                   ->willReturn($channel);

        return $connection;
    }
}
