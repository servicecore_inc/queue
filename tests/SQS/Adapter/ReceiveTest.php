<?php

namespace ServiceCore\Queue\Test\SQS\Adapter;

use Aws\Result;
use Aws\Sqs\SqsClient;
use Laminas\EventManager\ResponseCollection;
use PHPUnit\Framework\TestCase;
use ServiceCore\Queue\Core\Data\Message;
use ServiceCore\Queue\Core\Event\PostReceived;
use ServiceCore\Queue\Core\Event\Received;
use ServiceCore\Queue\Core\EventManager;
use ServiceCore\Queue\SQS\Adapter\Receive;

class ReceiveTest extends TestCase
{
    public function testInvoke(): void
    {
        $queueUrl = 'queue.url.invalid';
        $endpoint = 'http://foo.bar';
        $target   = 'target.invalid';
        $body     = [
            'body' => true
        ];

        $connectionParameters = [
            'AttributeNames'        => ['SentTimestamp'],
            'MaxNumberOfMessages'   => 1,
            'MessageAttributeNames' => ['All'],
            'WaitTimeSeconds'       => 10,
            'QueueUrl'              => "${endpoint}/${queueUrl}",
            'MessageGroupId'        => 'fooBar'
        ];

        $receiptHandle = 'Test123';
        $sqsClient     = $this->getMockClient($target, $receiptHandle, $body, $connectionParameters);
        $receive       = new Receive($sqsClient, $endpoint, $connectionParameters);

        $evmBuilder = $this->getMockBuilder(EventManager::class);

        $evmBuilder->disableOriginalConstructor();
        $evmBuilder->setMethods(
            [
                'trigger'
            ]
        );

        $eventManager = $evmBuilder->getMock();
        $received     = new Received(
            new Message(
                $target,
                $body,
                [
                    'x-message-id' => $receiptHandle
                ]
            )
        );

        $eventManager->expects($this->exactly(2))
                     ->method('trigger')
                     ->withConsecutive(
                         [
                             Received::class,
                             $received
                         ],
                         [
                             PostReceived::class,
                             $received
                         ]
                     )
                     ->willReturn(new ResponseCollection());

        $receive->setEventManager($eventManager);

        $receive($queueUrl, false);
    }

    private function getMockClient(
        string $target,
        string $receiptHandle,
        array $body,
        array $connectionParameters
    ): SqsClient {
        $clientBuilder = $this->getMockBuilder(SqsClient::class);

        $clientBuilder->disableOriginalConstructor();

        $clientBuilder->setMethods(
            [
                'receiveMessage',
                'deleteMessage'
            ]
        );

        $client   = $clientBuilder->getMock();
        $result   = new Result();
        $messages = [
            [
                'Body'              => \json_encode($body),
                'MessageAttributes' => [
                    'Target' => [
                        'DataType'    => 'String',
                        'StringValue' => $target
                    ]
                ],
                'ReceiptHandle'     => $receiptHandle
            ]
        ];

        $result->offsetSet('Messages', $messages);

        $client->expects($this->once())
               ->method('receiveMessage')
               ->with($connectionParameters)
               ->willReturn($result);

        $client->expects($this->once())
               ->method('deleteMessage')
               ->willReturn(true);

        return $client;
    }
}
