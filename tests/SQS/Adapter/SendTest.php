<?php

namespace ServiceCore\Queue\Test\SQS\Adapter;

use Aws\Sqs\SqsClient;
use PHPUnit\Framework\TestCase;
use ServiceCore\Queue\Core\Data\Message;
use ServiceCore\Queue\Core\Event\PreQueued;
use ServiceCore\Queue\Core\Event\Queued;
use ServiceCore\Queue\Core\EventManager;
use ServiceCore\Queue\SQS\Adapter\Send;

class SendTest extends TestCase
{
    public function testInvoke(): void
    {
        $queueName            = 'fooBar';
        $target               = 'test.target';
        $data                 = [];
        $endpoint             = 'http://fooBarEndpoint.com';
        $connectionParameters = [
            'MessageAttributes' => [
                'Target' => [
                    'DataType'    => 'String',
                    'StringValue' => $target
                ]
            ],
            'MessageBody'       => \json_encode($data),
            'QueueUrl'          => "{$endpoint}/${queueName}"
        ];

        $sqsClient  = $this->getMockClient($queueName, $endpoint, $target);
        $send       = new Send($sqsClient, $endpoint, $connectionParameters);
        $evmBuilder = $this->getMockBuilder(EventManager::class);

        $evmBuilder->disableOriginalConstructor();
        $evmBuilder->onlyMethods(
            [
                'trigger'
            ]
        );

        $eventManager = $evmBuilder->getMock();

        $eventManager->expects($this->exactly(2))
                     ->method('trigger')
                     ->withConsecutive(
                         [
                             PreQueued::class,
                             new PreQueued()
                         ],
                         [
                             Queued::class,
                             new Queued(new Message($target, $data))
                         ]
                     )
                     ->willReturn(true);

        $send->setEventManager($eventManager);

        $send($queueName, $target, $data);
    }

    public function testInvokeWithFifo(): void
    {
        $queueName            = 'fooBar';
        $target               = 'test.target.fifo';
        $data                 = [];
        $properties           = ['x-message-group-id' => '123'];
        $endpoint             = 'http://fooBarEndpoint.com';
        $connectionParameters = [
            'MessageAttributes' => [
                'Target' => [
                    'DataType'    => 'String',
                    'StringValue' => $target
                ]
            ],
            'MessageBody'       => \json_encode($data),
            'QueueUrl'          => "{$endpoint}/${queueName}"
        ];

        $sqsClient  = $this->getMockClient($queueName, $endpoint, $target);
        $send       = new Send($sqsClient, $endpoint, $connectionParameters);
        $evmBuilder = $this->getMockBuilder(EventManager::class);

        $evmBuilder->disableOriginalConstructor();
        $evmBuilder->onlyMethods(
            [
                'trigger'
            ]
        );

        $eventManager = $evmBuilder->getMock();

        $eventManager->expects($this->exactly(2))
            ->method('trigger')
            ->withConsecutive(
                [
                    PreQueued::class,
                    new PreQueued()
                ],
                [
                    Queued::class,
                    new Queued(new Message($target, $data))
                ]
            )
            ->willReturn(true);

        $send->setEventManager($eventManager);

        $send($queueName, $target, $data, $properties);
    }

    private function getMockClient(string $queueName, string $endpoint, string $target): SqsClient
    {
        $clientBuilder = $this->getMockBuilder(SqsClient::class);

        $clientBuilder->disableOriginalConstructor();

        $clientBuilder->addMethods(
            [
                'sendMessage',
            ]
        );

        $client = $clientBuilder->getMock();

        $client->expects($this->once())
               ->method('sendMessage')
               ->with(
                   [
                       'MessageAttributes' => [
                           'Target' => [
                               'DataType'    => 'String',
                               'StringValue' => $target
                           ]
                       ],
                       'MessageBody'       => \json_encode([]),
                       'QueueUrl'          => $endpoint . '/' . $queueName
                   ]
               )
               ->willReturn(true);

        return $client;
    }

    private function getMockClientFifo(string $queueName, string $endpoint, string $target): SqsClient
    {
        $clientBuilder = $this->getMockBuilder(SqsClient::class);

        $clientBuilder->disableOriginalConstructor();

        $clientBuilder->addMethods(
            [
                'sendMessage',
            ]
        );

        $client = $clientBuilder->getMock();

        $client->expects($this->once())
               ->method('sendMessage')
               ->with(
                   [
                       'MessageAttributes'      => [
                           'Target' => [
                               'DataType'    => 'String',
                               'StringValue' => $target
                           ]
                       ],
                       'MessageBody'            => \json_encode([]),
                       'QueueUrl'               => $endpoint . '/' . $queueName,
                       'MessageDeduplicationId' => 'abc-123',
                       'MessageGroupId'         => '123',
                   ]
               )
               ->willReturn(true);

        return $client;
    }
}
